import requests
from bs4 import BeautifulSoup


def to_soup(link):
    try:
        r = requests.get(link)
    except requests.exceptions.MissingSchema:
        r = requests.get(f"https://{link}")
    return BeautifulSoup(r.text, 'lxml')