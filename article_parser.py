import spacy
nlp = spacy.load("en_core_web_lg")


class Location(object):

    def _find_phone_number(self):
        pass

    def _find_address(self):
        pass

    def __str__(self):
        return f"{self.name} {self.cost} {self.url} \n\n"

    def __repr__(self):
        return str(self)

class NewsArticle(object):
    def __init__(self, soup):
        self.soup = self._get_whole_body(soup)
        self.locations = []

    def _get_whole_body(self, soup):
        return soup.find("section", {"name":"articleBody"})

    def _get_header_time(self, header):
        doc = nlp(header)
        for ent in doc.ents:
            if ent.label_ == "TIME":
                time = ent.text
                return time

    def _extract_body_costs(self, body):
        doc = nlp(body)
        costs = [ent.text \
                 for ent in doc.ents \
                 if ent.label_ == "MONEY"]
        return "/".join(costs)

    def _extract_body_links(self, body):
        links = body.find_all("a")
        if links:
            name = links[0].text
            url = links[0].get("href")
            if len(links) > 1:
                additional_places = [(link.text, link.get("href")) \
                                    for link in links[1:]]
            else:
                additional_places = None

            return name, url, additional_places
        else:
            return None, None, None

    def iterate_tags(self, header_tag="h3", body_tag="p"):
        for header in self.soup.find_all(header_tag):
            l = Location()

            body = header.find_next(body_tag)
            l.article_copy = body.text
            l.name, l.url, l.additional_places = self._extract_body_links(body)
            l.cost = self._extract_body_costs(body.text)
            l.time = self._get_header_time(header.text[3:])
            self.locations.append(l)

        return self.locations