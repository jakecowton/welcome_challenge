import json
import re

from transformers import pipeline

from utils import to_soup


STRIPPING_REGEX = r" {2,}.* {2,}"
NLP_QA = pipeline('question-answering')


class Review(object):

    def __init__(self, review):
        self.review_data = review
        self.load_json(self.review_data)

    def load_json(self, review):
        self.text = self._clean_review_body(review.get("reviewBody"))
        self.rating = review.get("reviewRating").get("ratingValue")
        self.keywords = review.get("keywords")
        self._extract_district(review)

    def _clean_review_body(self, text):
        text = text.strip().replace("\n", "")
        text = re.sub(STRIPPING_REGEX, "", text)
        return text

    def _extract_district(self, review):
        districts = review.get("itemReviewed").get("additionalProperty").get("value")
        self.districts = " / ".join(districts)


class Restaurant(object):

    def __init__(self, item):
        self.load_json(item)

    def load_json(self, item):
        self.name = item.get("name")
        self.address = item.get("item").get("address")
        self.cost = item.get("item").get("priceRange")
        # TODO
        self.food_type = None
        self.neighbourhood = None
        self.description = item.get("description")
        self.url = item.get("url")
        self.image = item.get("image")

        if item.get("item").get("review"):
            self.review_link = item.get("item").get("review").get("url")
            self.parse_review(self.review_link)

            self.neighbourhood = self.review.districts
            self.score = self.review.rating
            self.keywords = self.review.keywords

        else:
            self.review_link = None

    def parse_review(self, url):
        review_soup = to_soup(url)
        self.review = LDJson(review_soup).extract_data()

    def qa(self, question):
        """
        Ask a question about the location
        """
        if self.review_link:
            qa = NLP_QA({"question":question,
                         "context":self.review.text})
            return qa.get("answer")
        else:
            return "Sorry, there isn't a review for this location yet."

    def __str__(self):
        if self.review_link is None:
            return f"{self.name} | {self.address} | No review listed\n\n"
        else:
            return f"{self.name} | {self.neighbourhood} | {self.score} | {self.keywords}\n\n"

    def __repr__(self):
        return str(self)

class LDJson(object):

    def __init__(self, soup):
        self.soup = soup
        self.data = self.ld_json_extract(self.soup)
        self.type = self.data.get("@type")

    def extract_data(self):
        if self.type == "Review":
            return self.extract_review()
        elif self.type == "ItemList":
            return self.extract_listicle()

    def extract_review(self):
        return Review(self.ld_json_extract(self.soup))

    def extract_listicle(self):
        locations = self.ld_json_extract(self.soup).get("itemListElement")
        return [Restaurant(location_data) for location_data in locations]

    def ld_json_extract(self, soup):
        sc = soup.find("script", {"type":"application/ld+json"})
        data = self._ld_json_soup_to_json(sc)
        return data

    def _ld_json_soup_to_json(self, tag):
        return json.loads(tag.text)