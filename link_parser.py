from ld_json_parser import LDJson
from article_parser import NewsArticle

from utils import to_soup


class LinkParser(object):

    def __init__(self, link):
        self.soup = to_soup(link)

    def parse(self):
        if self.soup.find("script", {"type":"application/ld+json"}):
            article = LDJson(self.soup)
            if article.type == "ItemList":
                locations = article.extract_data()
                return locations
            if article.type == "NewsArticle":
                article = NewsArticle(self.soup)
                locations = article.iterate_tags(header_tag="h3", body_tag="p")
                return locations


if __name__ == "__main__":
    la_link = "https://www.theinfatuation.com/los-angeles/guides/restaurants-for-last-meal-in-la"
    berlin_link = "https://www.nytimes.com/2019/10/31/travel/what-to-do-36-Hours-in-Berlin.html"

    lp = LinkParser(la_link)
    la_locations = lp.parse()

    lp = LinkParser(berlin_link)
    berlin_locations = lp.parse()