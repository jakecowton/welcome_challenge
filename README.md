# Welco.me Challenge

**Demo script below**

## Brief

We would like you to take a two articles and pull out the relevant data in
a manor of your choice. Please spend no more than five hours working on
this project:

Article 1:
https://www.theinfatuation.com/los-angeles/guides/restaurants-for-last-meal-in-la

Place Data for each place:

   - Names
   - Address
   - Cost
   - Food type
   - Neighbourhood
   - Image URL associated with the place
   - Relevant Article copy
   - Score
   - Group subtitle, example: WHEN YOU WANT TO BALL OUT ONE LAST TIME

Article 2:
https://www.nytimes.com/2019/10/31/travel/what-to-do-36-Hours-in-Berlin.html

Place data for each place:

   - Name
   - Relevant article copy
   - URL
   - Any image associated with the place
   - Relevant time
   - Relevant places - groups of place mentioned together
   - Phone
   - Address
   - Cost

It is important for us to understand the choices you make and why. We are
looking for you to be creative in your approach.

## Demo script

```bash
$ mkvirualenv -p python3.8 welcome

$ pip install -r requirements.txt
$ python -m spacy download en_core_web_lg # ~ 820 MB

$ python -i link_parser.py
# Necessary transformer models will be download
```
```python
bestia = la_locations[0]

print(bestia)
# Bestia | Arts District / Downtown LA | 9.1 | ['Arts District', 'Downtown LA', 'Italian', 'Corporate Cards', 'Date Night', 'Fine Dining', 'Impressing Out of Towners', 'Special Occasions']

bestia.qa("Where is it?")
# Arts District

bestia.qa("What is it called?")
# Bestia's

bestia.qa("What is the best dish?")
# Cacao crust, salted caramel, and olive oil.

for l in la_locations:
    print(l)
```
